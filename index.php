<?php
define('_RAIZ_',''); # Definición de la raíz

require_once _RAIZ_ . 'php/config.php'; # Configuraciones

require_once _RAIZ_ . 'php/rutas.php'; # Rutas

/*
$str = "Cósas que paSán";
echo mb_convert_case($str, MB_CASE_UPPER, "UTF-8");
echo mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
echo mb_convert_case($str, MB_CASE_LOWER, "UTF-8");
*/
