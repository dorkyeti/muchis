<?php
# Configuración de la base de datos para hacer el crud
$con = new mysqli('localhost', 'root', '', 'muchiscafe', 3306);
$con->set_charset("utf8");


# Helpers del CRUD
/**
 * Escapa los caracteres maliciosos para la base de datos
 */
function s(string $var) : string {
  return $GLOBALS['con']->real_escape_string($var);
}

/**
 * Crea la sentencia sql y la ejecuta para insertar datos, ya curados
 */
function insertar(string $tabla, array $array) {
  $sql = "INSERT INTO {$tabla} (";
  $valores = " VALUES (";

  foreach($array as $campo => $valor){
    $sql .= $campo . ', ';
    $valores .= '\'' . s($valor) . '\', ';
  }
  $sql[strlen($sql) - 2] = ')';
  $valores[strlen($valores) - 2] = ')';

  $sql .= $valores;

  return create($sql);
}

/**
 * Crea sentencia sql para actualizar X campos de una tabla de la base de datos, ya curados
 */
function actualizar(string $tabla, array $array, string $where = null){
  $sql = "UPDATE {$tabla} SET ";

  foreach ($array as $campo => $valor) {
    $sql .= $campo . ' = \'' . s($valor) . '\', ';
  }

  $sql[strlen($sql) - 2] = ' ';

  return update($sql . ($where != null ? 'WHERE ' . $where : ''));
}

# CRUD
/**
 * Selecciona los datos de la base de datos según el sql
 */
function read(string $sql){
  $query = $GLOBALS['con']->query($sql);
  if($query != false && $query->num_rows){
    $arreglo = $query->fetch_all(MYSQLI_ASSOC);
    $query->free();
    return $arreglo;
  }
  return [];
}

/**
 * Insertar un registro en la base de datos
 */
function create(string $sql) : int {
  $GLOBALS['con']->query($sql);
  return $GLOBALS['con']->insert_id;
}

/**
 * Actualizar un registro en la base de datos
 */
function update(string $sql) : int {
  $GLOBALS['con']->real_query($sql);
  return $GLOBALS['con']->affected_rows;
}

/**
 * Dumpea la conexión actual mysql (Depuración)
 */
function dump_mysql(){
  echo "\n<br>\n";
  return var_dump($GLOBALS['con']);
}

/**
 * Registra las acciones de los usuarios en la base de datos
 */
function historial(string $accion){
  return create("INSERT INTO historial(id, evento, fecha, hora, usuario_id) VALUES (NULL, '" . s($accion) . "', '".date('Y-m-d')."', '".date('H:i:s')."', '{$_SESSION['id']}')");
}