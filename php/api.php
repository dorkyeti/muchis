<?php
# Cosas de la api
header("Access-Control-Allow-Origin: ".BASE_HREF);
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: Wallace");
header('Content-Type: application/json');
# La api es la única que tiene (y debe) tener acceso a la lógica y base de datos del sistema. 
require_once _RAIZ_ . 'php/crud.php';
require_once _RAIZ_ . 'php/autoload.php';
# Escoge el "json" que se le solicita a la url y luego la acción
switch($api){
  # Cuando la api, no tenga ninguna solicitud de json. Retornará un arreglo json vacío.
  case NULL :
    return json([]);
    break;

  # Productos
  case 'productos.json':
    $productos = new Productos;

    if(isset($action_api) && $action_api === 'categorias') return json($productos->categorias); # Retorna las categorias

    if($_SERVER['REQUEST_METHOD'] === 'POST'){ # Actualizar o registrar
      if(isset($_POST['id']) && is_numeric($_POST['id'])) {
        return json($productos->actualizar());
      }

      return json($productos->registrar());
    } else if ($_SERVER['REQUEST_METHOD'] === 'GET') { # Solo mostrar
      if(isset($_GET['_']) && !isset($_GET['id'])){
        return json(['data' => $productos->todos]); # Retorna los datos para el datatable
      } else if(isset($_GET['id'])) {
        return json(['data' => $productos->getAll($_GET['id'])]);
      } else {
        return json($productos->todos); # Retorna los datos para todo lo demás
      }
    }
    break;

  # Usuarios
  case 'usuarios.json' :
    $usuarios = new Usuarios;

    if(isset($action_api) && $action_api === 'login' && $_SERVER['REQUEST_METHOD'] === 'POST') return json($usuarios->login());
    break;

  # Historial
  case 'historial.json' :
    if(isset($_GET['_'])) return json(['data' => (new Usuarios)->historial]);
    return json( (new Usuarios)->historial );
    break;

  # Entrada
  case 'entradas.json' :
    $inventario = new Inventario;
    if($_SERVER['REQUEST_METHOD'] === 'POST') {
      return json($inventario->reg_in());
    } else if($_SERVER['REQUEST_METHOD'] === 'GET'){
      
      if(isset($_GET['_']) && !isset($_GET['id'])) {
        return json([ 'data' => $inventario->getEntradas() ]);
      } else if(isset($_GET['id']) && !isset($_GET['_'])) {
        return json($inventario->getEntradas($_GET['id']));
      } else if(isset($_GET['id'])) {
        return json([ 'data' => $inventario->getEntradas($_GET['id']) ]);
      }
      return json($inventario->getEntradas());
    }
    break;

  # Salida
  case 'salidas.json' :
    $inventario = new Inventario;
    if($_SERVER['REQUEST_METHOD'] === 'POST') {
      return json($inventario->reg_out());
    } else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      
      if(isset($_GET['_']) && !isset($_GET['id'])) {
        return json([ 'data' => $inventario->getSalidas() ]);
      } else if(isset($_GET['id']) && !isset($_GET['_'])) {
        return json($inventario->getSalidas($_GET['id']));
      } else if(isset($_GET['id'])) {
        return json([ 'data' => $inventario->getSalidas($_GET['id']) ]);
      }
      return json($inventario->getSalidas());
    }
    break;
  
  # Proveedores
  case 'proveedores.json' :
    if(isset($_GET['_'])) return json(['data' => (new Inventario)->proveedores]);
    return json((new Inventario)->proveedores);
    break;

  # Inventario
  case 'inventario.json' :
    $inventario = new Inventario;
    if(isset($_GET['_'])) return json(['data' => $inventario->actual()]);

    return json($inventario->actual());
    break;

  # Testing el endpoint de cualquier clase
  case 'test.json' :
    # return json((new Inventario)->cod_in(20)); 
    # return json((new Inventario)->actual());
    # return json((new Inventario)->test());
    sleep(2);
    return json($_POST);
    break;
}