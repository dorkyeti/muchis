<?php
# Router de php
switch($ruta){
  case NULL :
  # Inicio
  if(empty($_SESSION['activo'])) return redir('login');
  return require_once _RAIZ_ . 'html/index.html';
  break;

  case 'login' :
  # Login
  if(isset($_SESSION['activo'])) return redir();
  return require_once _RAIZ_ . 'html/login.html';
  break;

  case 'pages' :
  if($_SERVER['REQUEST_METHOD'] === 'POST') {
    switch($api){
      case 'home': return require_once _RAIZ_ . 'html/home.html'; break;
      case 'inventario': return require_once _RAIZ_ . 'html/mercancia.html'; break;
      case 'nuevo': return require_once _RAIZ_ . 'html/nuevo.html'; break;
      case 'producto_detalles': return require_once _RAIZ_ . 'html/entrada_salida.html'; break;
      case 'requerimiento': return require_once _RAIZ_ . 'html/requerimiento.html'; break;
      case 'seguimiento': return require_once _RAIZ_ . 'html/seguimiento.html'; break;
    }
  }
  else return;
  break;

  case 'api' :
  # if(isXmlHttpRequest()) 
  return require_once _RAIZ_ . 'php/api.php';
  # else return print 'Solo peticiones ajax';
  break;

  case 'logout' :
  unset($_SESSION);
  session_destroy();
  redir('login');
  break;

  case 'info' :
  # Depuración
  phpinfo();
  break;

  default :
  # Error 404
  return print "Error 404";
  break;
}
