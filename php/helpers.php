<?php
/**
 * Retorna la información en un json
 */
function json($var) {
  return print json_encode($var);
}

/**
 * Maneja la sesión con una función
 */
function session(string $indice = NULL, $valor = NULL) {
  if($indice == NULL) return $_SESSION;

  if($valor == NULL){
    return isset($_SESSION[$indice]) ? $_SESSION[$indice] : false;
  }else{
    $_SESSION[$indice] = $valor;
    return $_SESSION[$indice];
  }
}

/**
 * Verifica si es una petición AJAX lo que se está solicitando
 */
function isXmlHttpRequest(){
  return ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : NULL) === 'XMLHttpRequest');
}

/**
 * Redirecciona
 */
function redir(string $url = NULL) {
	return header("Location: ".BASE_HREF."{$url}");
  // return print '<meta http-equiv="refresh" content="0;url=http://localhost/munchis/'.$url.'"/>';
}
