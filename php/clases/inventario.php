<?php
/**
 * Clase encargada de llevar la lógica del inventario, la entrada y la salida.
 */
class Inventario {
	
	function __construct() {
		$this->proveedores = read("SELECT nombre AS nom FROM proveedores");
	}

	/**
	 * Registra o busca a un proveedor
	 *
	 * @return integer
	 */
	private function proveedor(string $nom) : string {
		# Limpia y prepara el nombre del proveedor
		$nombre = mb_convert_case($nom, MB_CASE_UPPER, "UTF-8");

		# Busca si el proveedor existe
		$proveedor = read("SELECT id FROM proveedores WHERE nombre = '{$nombre}'");

		if($proveedor == false) {
			$return = insertar('proveedores', ['nombre' => $nombre]); # Si no existe se registra
		} else {
			$return = $proveedor[0]['id']; # Si existe se pide su id
		}

		return $return; # Se retorna el id de ambas formas
	}

	/**
	 * Obtiene un codigo de un producto para registrar su entrada 
	 *
	 * @return string 
	 */
	private function cod_in($id) : string {
		return read("SELECT CONCAT(p.cod, '-', COUNT(i.id_producto)+1) AS cod FROM productos AS p INNER JOIN inventario AS i ON i.id_producto = p.id WHERE p.id = '{$id}'")[0]['cod'];
	}

	/**
	 * Obtiene los datos actual del inventario
	 *
	 * @return array
	 */
	public function actual() {
		$actual = [];
		$pros =  read("SELECT p.id, p.cod, p.nombre AS nom, p.descripcion AS des, p.cantidad AS cant, p.al_min AS min, p.al_max AS max, CONCAT_WS(' ', p.al_min, '/', p.al_max, p.medida) AS minmax, p.medida AS uni, u.nombre, c.categoria AS cat, RIGHT(cod, 5) AS cod_wo FROM productos AS p INNER JOIN categorias AS c ON p.categoria = c.id INNER JOIN usuarios AS u ON p.id_usuario = u.id");

		foreach ($pros as $pro) {
		# id, cod, nombre, descripcion, cantidad, min, max, minmax, madida, nombre, categoria, codigo, stock
			$stock = (int) read("SELECT SUM(stock) AS stock  FROM inventario WHERE id_producto = {$pro['id']}")[0]['stock'];
			$actual[] = ['id' => $pro['id'], 'cod' => $pro['cod'], 'codigo' => $pro['cod_wo'], 'nom' => $pro['nom'], 'cant' => (int) $pro['cant'], 'min' => (int) $pro['min'], 'max' => (int) $pro['max'], 'cat' => $pro['cat'], 'minmax' => $pro['minmax'], 'uni' => $pro['uni'], 'usu' => $pro['nombre'], 'stock' => $stock . ' ' . $pro['uni'], 'stock_' => $stock, 'des' => $pro['des']];
		}
		return $actual;
	}

	# TODO: Rehacer función
	/**
	 * Registrar la entrada de un producto en el inventario
	 *
	 * @return array
	 */
	public function reg_in() : array {
		extract($_POST); # Extracción de datos

		$proveedor = $this->proveedor($proveedor); # Busqueda del proveedor

		$pro = read("SELECT nombre, unidad FROM productos WHERE id = '{$producto}'")[0]; # Busqueda del id del producto

		$cod = $this->cod_in($producto); # Busqueda del código del producto

		$inv = insertar('inventario', [
			'cantidad' => $cantidad,
			'stock' => ($cantidad*$unidad),
			'fecha_e' => $fecha_e,
			'id_proveedor' => $proveedor,
			'id_producto' => $producto,
			'precio' => $precio,
			'fecha_v' => $fecha_v,
			'codigo' => $cod,
			'unidad' => $unidad
 		]);

		if($inv > 0){
			historial('Registro la entrada del producto ('.$pro['nombre'].') de '.($cantidad*$unidad).' '.$pro['unidad'].' #'.$inv);
			return ['success' => 1, 'msj' => 'Se registro la entrada con exito'];
		} else {
			return ['success' => 0, 'msj' => 'Error lógico'];
		}
	}

	/**
	 * Obtiene las entradas de todos los productos en el inventario o de un producto en especifico
	 *
	 * @param $id: El id del producto al que sacarle las entradas
	 * @return array
	 */
	public function getEntradas($id = NULL) : array {
		$sql = "SELECT i.id, i.codigo AS cod, DATE_FORMAT(i.fecha_e, '%d-%m-%Y') AS fecha_e, pr.nombre AS proveedor, DATE_FORMAT(i.fecha_v, '%d-%m-%Y') AS fecha_v, i.precio, p.unidad AS uni, i.cantidad AS cant, i.stock FROM inventario AS i INNER JOIN productos AS p ON i.id_producto = p.id INNER JOIN proveedores AS pr ON i.id_proveedor = pr.id";
		if($id != NULL) $sql .= " WHERE p.id = '".s($id)."'";
		return read($sql);
	}

	/**
	 * Registra la salida de un producto del inventario actual
	 *
	 * @return array
	 */
	public function reg_out() : array {
		extract($_POST);

		$val = read("SELECT SUM(stock) AS stock FROM inventario WHERE id_producto = '{$producto}'")[0]['stock'];

		$pro = read("SELECT nombre, unidad FROM productos WHERE id = '{$producto}'")[0];

		$total = $cantidad*$unidad;

		if($total > $val) return ['success' => 0, 'msj' => 'La cantidad que se está solicitando excede la cantidad disponible en el inventario'];

		$inv = read("SELECT * FROM inventario WHERE id_producto = '{$producto}' AND stock != 0 ORDER BY fecha_v ASC");

		$cant = $total;
		foreach ($inv as $stock) {
			if($stock['stock'] < $cant){
				$cant -= $stock['stock'];
				actualizar('inventario', ['stock' => 0], "id = '{$stock['id']}'");
			} else {
				$cantSql = $stock['stock'] - $cant;
				actualizar('inventario', ['stock' => $cantSql], "id = '{$stock['id']}'");
				break; 
			}
		}

		$salida = insertar('salida', [
			'unidad' => $unidad,
			'cantidad' => $cantidad,
			'fecha_s' => $fecha_s,
			'hora' => date('H:i:s'), # Arreglar esto en la vista
			'descripcion' => $transporte, # Arreglar en la vista
			'destino' => $destino,
			'estatus' => 'Pendiente',
			'producto_id' => $producto,
			'usuario_id' => $_SESSION['id']
		]);

		if($salida > 0){
			historial("Registro una salida de {$total} {$pro['unidad']} del producto {$pro['nombre']}");
			return ['success' => 1, 'msj' => 'Salida registrada con exito'];
		} else {
			return ['success' => 0, 'msj' => 'Error lógico'];
		}
	}

	/**
	 * Obtiene las salidas de todos los productos en el inventario o de un producto en especifico
	 *
	 * @param $id: EL id del producto al que sacarles las salidas
	 * @return array
	 */
	public function getSalidas($id = NULL) : array {
		$sql = "SELECT s.id, p.cod, s.fecha_s, s.estatus, s.unidad, s.cantidad, s.descripcion, (s.cantidad*s.unidad) AS total, u.nombre AS usu FROM salida AS s INNER JOIN usuarios AS u ON s.usuario_id = u.id INNER JOIN productos AS p ON s.producto_id = p.id";
		if($id != NULL) $sql .= " WHERE p.id = '". s($id) ."'";
		return read($sql);
	}
}