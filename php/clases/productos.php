<?php
/**
 * Clase encargada de llevar la lógica de productos
 */
class Productos {
  function __construct() {
    $this->todos = read("SELECT p.id, p.cod, p.nombre AS nom, p.al_min AS min, p.al_max AS max, c.categoria AS cat, p.medida, p.cantidad, u.nombre AS persona, IFNULL((SELECT SUM(i.stock) AS stock FROM inventario AS i WHERE i.id_producto = p.id), 0) AS stock FROM productos AS p INNER JOIN categorias AS c ON p.categoria = c.id INNER JOIN usuarios AS u ON p.id_usuario = u.id");
    $this->categorias = read("SELECT * FROM categorias");
  }

  /**
   * Crea un código en caso tal de que no exista
   *
   * @return string
   */
  private function codigo() : string {
    do {
      $cod = '#' . rand(10000,99999);
    } while(read("SELECT * FROM productos WHERE cod = '{$cod}'") != false);

    return $cod;
  }

  /**
   * Registra o busca una categoria
   *
   * @return integer
   */
  private function categoria(string $nombre) : int {
    # Limpia y prepara la categoria
    $nombre = ucfirst(mb_convert_case($nombre, MB_CASE_LOWER, "UTF-8"));

    # Busca si existe la categoria
    $categoria = read("SELECT id FROM categorias WHERE categoria = '{$nombre}'");

    if($categoria == false){
      # Si no existe la categoria, se registra
      $return = insertar('categorias', ['categoria' => $nombre]);
    } else {
      # Si existe, se toma su id
      $return = $categoria[0]['id'];
    }
    # De ambas formas se retorna el id de la categoria
    return $return;
  }

  /**
   * Registra un nuevo producto
   *
   * @return array
   */
  public function registrar() : array {
    extract($_POST); # Extraer valores

    $nombre = ucfirst(mb_convert_case($nombre, MB_CASE_LOWER, "UTF-8"));
    if(read("SELECT * FROM productos WHERE nombre = '{$nombre}'") != false) return ['success' => 0, 'msj' => 'Ya existe ese producto'];

    if(read("SELECT * FROM productos WHERE cod = '{$cod}'") != false) return ['success' => 0, 'msj' => 'Ese código está siendo usado por otro producto'];

    $categoria = $this->categoria($categoria); # Se encarga automaticamente de la categoria

    if(empty($cod)) $cod = $this->codigo(); # Genera automaticamente un código para el producto en caso tal del que el usuario no lo haya ingresado
    else { $cod = '#' . $cod; }
    
    # Se registra el producto
    $producto = insertar('productos', [
      'cod' => $cod,
      'nombre' => $nombre,
      'descripcion' => $descripcion,
      'al_max' => $al_max,
      'al_min' => $al_min,
      'medida' => $unidad_medida,
      'cantidad' => $unidad_caja,
      'categoria' => $categoria,
      'id_usuario' => $_SESSION['id']
    ]);

    # Se valida es la confirmación
    if($producto > 0){
      historial('Registro el producto '.$cod.' bajo el nombre de '.$nombre);
      return ['success' => 1, 'msj' => 'Producto registrado con exito'];
    } else {
      return ['success' => 0, 'msj' => 'No se pudo registrar el producto'];
    }
  }

  /**
   * Actualiza los datos de un producto
   *
   * @return array
   */
  public function actualizar() : array {
    extract($_POST);

    $nombre = ucfirst(mb_convert_case($producto, MB_CASE_LOWER, 'UTF-8'));
    $categoria = $this->categoria($categoria);
    # Investigar como modificar el codigo de un producto 

    $act = actualizar('productos', [
      'cod' => '#' . $cod,
      'nombre' => $producto,
      'descripcion' => $descripcion,
      'al_max' => $al_max,
      'al_min' => $al_min,
      'medida' => $unidad_medida,
      'cantidad' => $cantidad,
      'categoria' => $categoria
    ], "id = '{$id}'");

    if($act) {
      historial("Modifico al producto {$producto} (#{$cod})");
      return ['success' => 1, 'msj' => 'Se ha modificado el producto'];
    } else {
      return ['success' => 0, 'msj' => 'No se ha modificado nada'];
    }
  }
}