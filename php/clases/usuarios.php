<?php
/**
 * 
 */
class Usuarios {
	
	function __construct() {
		$this->todos = read("SELECT * FROM usuarios");
		$this->historial = read("SELECT h.evento, DATE_FORMAT(h.fecha, '%d-%m-%Y') AS fecha, DATE_FORMAT(h.hora, '%h:%i %p') AS hora, u.usuario FROM historial AS h INNER JOIN usuarios AS u ON h.usuario_id = u.id");
	}

	/**
	 * Login (Genera la credenciales de acceso al usuario)
	 * 
	 * @return array
	 */
	public function login() : array {
		$user = s($_POST['user']); # Limpia al usuario (Evita inyecciones SQL)

		$usuario = read("SELECT * FROM usuarios WHERE usuario ='{$user}'"); # Busca al usuario

		if($usuario === []) return ['success' => 0, 'msj' => 'No hay ningún usuario registrado con ese nombre']; # Retorna error si no hay un usuario
		
		$usuario = $usuario[0];

		if($usuario['estatus'] === 'Inactivo') return ['success' => 0, 'msj' => 'El usuario está inactivo'];

		if(!password_verify($_POST['pass'], $usuario['clave'])) return ['success' => 0, 'msj' => 'La contraseña no coincide']; # Retorna error si existe el usuario pero la contraseña no coincide

		$_SESSION = [
			'activo' => true,
			'id' => $usuario['id'], 
			'usuario' => $usuario['usuario'],
			'tipo' => $usuario['tipo'],
			'nombre' => $usuario['nombre']
		];
		historial('Inicio sesión');
		return ['success' => 1];
	}

	/**
	 * Registro de un usuario
	 *
	 * @return array
	 */
	public function registrar() : array {
		extract($_POST);
		$usuario = s($usuario);

		if(read("SELECT * FROM usuarios WHERE usuario = '{$usuario}'") != false) return ['success' => 0, 'msj' => 'Ya hay un usuario con ese nombre'];

		$usuario = insertar('usuarios', [
			'usuario' => $usuario,
			'clave' => password_hash($pass, PASSWORD_DEFAULT),
			'tipo' => $tipo,
			'nombre' => mb_convert_case($nombre, MB_CASE_TITLE, "UTF-8"),
			'fecha' => date('Y-m-d')
		]);

		if($usuario > 0) {
			historial('Registro al usuario : '.$usuario);
			return ['success' => 1, 'msj' => 'Se registro con existo'];
		} else {
			return ['success' => 0, 'msj' => 'Error lógico'];
		}
	}
}