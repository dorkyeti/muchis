<?php
# Autoload
spl_autoload_register(function($class){
  $ruta = _RAIZ_ . 'php/clases/' . strtolower($class) . '.php';
  if(is_readable($ruta)) require_once $ruta;
});
