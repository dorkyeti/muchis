<?php
# Se establece el tiempo
date_default_timezone_set('America/Caracas');

# Se incia la sesión
session_name('munchiscafe');
session_start();

# Configuración del "Router"
$ruta = NULL;
if(isset($_GET["route"])){
  @list($ruta, $api, $action_api) = explode('/', $_GET["route"]);
}

# Helpers
require_once _RAIZ_ . 'php/helpers.php';

# Definición de constantes
define('BASE_HREF', 'http://localhost/muchis/');
define('TITLE', 'Muchis café');
