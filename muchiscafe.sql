-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-12-2018 a las 01:41:24
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `muchiscafe`
--
CREATE DATABASE IF NOT EXISTS `muchiscafe` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `muchiscafe`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_categoria` (`categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`) VALUES
(9, 'Agua'),
(1, 'Cereales'),
(6, 'Chocolate'),
(8, 'Cod'),
(4, 'Frutas'),
(2, 'Granos'),
(7, 'Jugos'),
(3, 'Lacteos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE IF NOT EXISTS `historial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evento` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `historial`
--

INSERT INTO `historial` (`id`, `evento`, `fecha`, `hora`, `usuario_id`) VALUES
(1, 'Inicio sesión', '2018-11-29', '17:02:06', 1),
(2, 'Inicio sesión', '2018-11-29', '17:04:29', 1),
(3, 'Inicio sesión', '2018-11-29', '22:21:00', 1),
(4, 'Inicio sesión', '2018-11-30', '14:31:43', 1),
(5, 'Inicio sesión', '2018-11-30', '15:08:15', 1),
(6, 'Inicio sesión', '2018-11-30', '15:41:44', 1),
(7, 'Inicio sesión', '2018-12-01', '01:45:44', 1),
(8, 'Inicio sesión', '2018-12-01', '02:05:42', 1),
(9, 'Inicio sesión', '2018-12-01', '02:08:23', 1),
(10, 'Inicio sesión', '2018-12-01', '02:09:47', 1),
(11, 'Inicio sesión', '2018-12-01', '02:13:51', 1),
(12, 'Inicio sesión', '2018-12-01', '02:14:07', 1),
(13, 'Inicio sesión', '2018-12-01', '13:16:35', 1),
(14, 'Inicio sesión', '2018-12-01', '16:44:23', 1),
(15, 'Inicio sesión', '2018-12-01', '16:45:27', 1),
(16, 'Inicio sesión', '2018-12-01', '17:06:16', 1),
(17, 'Inicio sesión', '2018-12-01', '17:33:01', 1),
(18, 'Inicio sesión', '2018-12-01', '17:34:48', 1),
(19, 'Registro la entrada del producto (Harina de trigo) de  Pcs #1', '2018-12-02', '16:33:01', 1),
(20, 'Registro la entrada del producto (Harina de trigo) de 30 Pcs #2', '2018-12-02', '16:33:36', 1),
(21, 'Registro la entrada del producto (Cacao) de 30 Kg #3', '2018-12-02', '17:34:12', 1),
(22, 'Inicio sesión', '2018-12-02', '20:13:44', 1),
(23, 'Registro la entrada del producto (Harina de trigo) de 360 Pcs #4', '2018-12-02', '21:41:17', 1),
(24, 'Registro la entrada del producto (Cacao) de 200 Kg #5', '2018-12-02', '21:44:09', 1),
(25, 'Inicio sesión', '2018-12-02', '23:26:59', 1),
(26, 'Inicio sesión', '2018-12-02', '23:27:11', 1),
(27, 'Inicio sesión', '2018-12-03', '15:20:09', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE IF NOT EXISTS `inventario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `fecha_e` date NOT NULL,
  `fecha_v` date NOT NULL,
  `precio` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `productos` (`id_producto`),
  KEY `inventario_proveedor` (`id_proveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`id`, `cantidad`, `stock`, `fecha_e`, `fecha_v`, `precio`, `id_producto`, `id_proveedor`) VALUES
(1, 30, 30, '2018-12-02', '2019-02-28', '123,98', 8, 3),
(2, 30, 30, '2018-12-02', '2019-02-28', '123,98', 8, 3),
(3, 30, 50, '2018-12-02', '2019-02-28', '123,98', 3, 3),
(4, 20, 360, '2018-12-02', '2019-06-30', '500.00', 8, 1),
(5, 20, 200, '2018-12-02', '2020-12-30', '200', 3, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod` varchar(7) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `max` int(10) UNSIGNED NOT NULL,
  `min` int(10) UNSIGNED NOT NULL,
  `unidad` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `categoria` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_que_agrego_el_producto` (`id_usuario`),
  KEY `producto_categoria` (`categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `cod`, `nombre`, `descripcion`, `max`, `min`, `unidad`, `categoria`, `id_usuario`) VALUES
(3, '#000039', 'Cacao', 'La harina es buena para ti', 30, 10, 'Kg', 6, 1),
(4, '#123495', 'Caramelos', 'La harina es buena para ti', 100, 30, 'Pcs', 6, 1),
(7, '#123456', 'Comida instantanea', 'La harina es bueno pa ti pa tu cuerpo pa too', 123, 123, 'Pcs', 2, 1),
(8, '#000001', 'Harina de trigo', 'La harina de trigo fue creada por un trigador', 100, 50, 'Pcs', 1, 1),
(9, '#234555', 'Oisjfioodijofi', 'ooisdjfoisjdiofsidf', 4294967295, 1938, 'Kg', 1, 1),
(10, '#129873', 'Oisfdofi', 'jiosdjfoisdjfoi', 98238, 1838, 'Pcs', 8, 1),
(11, '#294070', 'Culo', 'jiosdjfoisdjfoi', 98238, 1838, 'Pcs', 8, 1),
(12, '#483564', 'Coco', 'Es una fruta del caribe que te puedes comer', 100, 10, 'Pcs', 4, 1),
(13, '#193965', 'Pera', 'La pera es una fruta que viene de no se donde para que te la comas', 56, 1, 'Pcs', 4, 1),
(14, '#716307', 'Manzana', 'La manzana es una fruta', 50, 23, 'Pcs', 4, 1),
(19, '#385468', 'Cuca', 'Es la parte feminina de la mujer xd', 200, 100, 'Kg', 4, 1),
(20, '#276006', 'Agua mineral', 'Es mineral', 100, 50, 'Pcs', 9, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE IF NOT EXISTS `proveedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id`, `nombre`) VALUES
(1, 'Distribuidora Norte, C.A.'),
(2, 'Distribuidora Que te importa, C.A.'),
(3, 'DISTRIBUIDORA'),
(4, 'PROVEEDORA PROVEE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salida`
--

CREATE TABLE IF NOT EXISTS `salida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL,
  `fecha_s` date NOT NULL,
  `hora` time NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `destino` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `estatus` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `producto_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `salida_producto` (`producto_id`),
  KEY `salida_usuario` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` enum('Administrador','Operador','Cocinero') COLLATE utf8_spanish_ci NOT NULL,
  `estatus` enum('Activo','Inactivo') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Activo',
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_usuario` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `clave`, `tipo`, `estatus`, `nombre`, `fecha`) VALUES
(1, 'wjm', '$2y$10$rwuOEXUVjUO/P10twUIfa.NCC6Z3nD.PH9sMOf.9PbDiGrxbXHq8G', 'Administrador', 'Activo', 'Waldo Malavé', '2018-11-29');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `historial`
--
ALTER TABLE `historial`
  ADD CONSTRAINT `usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `inventario_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productos` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `producto_categoria` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_que_agrego_el_producto` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `salida`
--
ALTER TABLE `salida`
  ADD CONSTRAINT `salida_producto` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `salida_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
