var t,theme = ["red", "blue", "teal", "green", "cyan", "purple", "brown", "orange", "blue-grey"];
$(window).on("load", function() {
  if(!localStorage.getItem('theme')) {
  } else {
    t = localStorage.getItem("theme");
    $("body").attr("data-ma-theme",t)
  }
  for (let i = 0; i < theme.length; i++) {
    if(t==theme[i]){
      $("label.bg-indigo").removeClass("active")
      $("label.bg-"+theme[i]).addClass("active")
    }
  }
});
if ($("body").on("change", ".theme-switch input:radio", function() {
  var a = $(this).val();
  localStorage.setItem("theme", a );
  t = localStorage.getItem("theme");
  $("body").attr("data-ma-theme", t)
}));
  /* ----- funciones del menu de vertical -----  */

  $(function() {
    function a(a) {
      a.requestFullscreen ? a.requestFullscreen() : a.mozRequestFullScreen ? a.mozRequestFullScreen() : a.webkitRequestFullscreen ? a.webkitRequestFullscreen() : a.msRequestFullscreen && a.msRequestFullscreen()
    }
    $("body").on("click", "[data-ma-action]", function(b) {
      b.preventDefault();
      var c = $(this)
      , d = c.data("ma-action")
      , e = "";
      switch (d) {
        case "search-open":
        $(".search").addClass("search--toggled");
        break;
        case "search-close":
        $(".search").removeClass("search--toggled");
        break;
        case "aside-open":
        e = c.data("ma-target"),
        c.addClass("toggled"),
        $(e).addClass("toggled"),
        $(".content, .header").append('<div class="ma-backdrop" data-ma-action="aside-close" data-ma-target=' + e + " />");
        break;
        case "aside-close":
        e = c.data("ma-target"),
        $('[data-ma-action="aside-open"], ' + e).removeClass("toggled"),
        $(".content, .header").find(".ma-backdrop").remove();
        break;
        case "fullscreen":
        a(document.documentElement);
        break;
        case "print":
        window.print();
        break;
        case "clear-localstorage":
        swal({
          title: "Are you sure?",
          text: "This can not be undone!",
          type: "warning",
          showCancelButton: !0,
          confirmButtonColor: "#3085d6",
          confirmButtonText: "Yes, clear it",
          cancelButtonText: "No, cancel"
        }).then(function() {
          localStorage.clear(),
          swal("Cleared!", "Local storage has been successfully cleared", "success")
        });
        break;
        case "login-switch":
        e = c.data("ma-target"),
        $(".login__block").removeClass("active"),
        $(e).addClass("active");
        break;
        case "notifications-clear":
        b.stopPropagation();
        var f = $(".top-nav__notifications .listview__item")
        , g = f.length
        , h = 0;
        c.fadeOut(),
        f.each(function() {
          var a = $(this);
          setTimeout(function() {
            a.addClass("animated fadeOutRight")
          }, h += 150)
        }),
        setTimeout(function() {
          f.remove(),
          $(".top-nav__notifications").addClass("top-nav__notifications--cleared")
        }, 180 * g);
        break;
        case "toolbar-search-open":
        $(this).closest(".toolbar").find(".toolbar__search").fadeIn(200),
        $(this).closest(".toolbar").find(".toolbar__search input").focus();
        break;
        case "toolbar-search-close":
        $(this).closest(".toolbar").find(".toolbar__search input").val(""),
        $(this).closest(".toolbar").find(".toolbar__search").fadeOut(200)
      }
    })
    $( window ).on( 'hashchange' , function() {
      if ($('.navigation li a').attr('href') === location.hash) {
        $('.navigation li a').parents('li.navigation__sub').removeClass('navigation__sub--active navigation__sub--toggled')
        $('.navigation li a[href="'+location.hash+'"]').parents('li.navigation__sub').addClass('navigation__sub--active ')
        $('.navigation li a').parent('li').removeClass('navigation__active')
        $('.navigation li a[href="'+location.hash+'"]').parent('li').addClass('navigation__active')
      } else {
        $('.navigation li a').parents('li.navigation__sub').removeClass('navigation__sub--active navigation__sub--toggled')
        $('.navigation li a[href="'+location.hash+'"]').parents('li.navigation__sub').addClass('navigation__sub--active')
        $('.navigation li a').parent('li').removeClass('navigation__active navigation__sub--toggled')
        $('.navigation li a[href="'+location.hash+'"]').parent('li').addClass('navigation__active')
      }
    });
    if (!$('.navigation li a').attr('href') ===! location.hash) {
      $('.navigation li a[href="'+location.hash+'"]').parent('li').addClass('navigation__active')
      $('.navigation li a[href="'+location.hash+'"]').parents('li.navigation__sub').addClass('navigation__sub--active')
    }
  });

$("body").on("click", ".navigation__sub > a", function(a) {
  a.preventDefault(),
  $(this).parent().toggleClass("navigation__sub--toggled"),
  $(this).next("ul").slideToggle(250)
})


/* ----- funciones de formularios -----  */
$(".form-group--float")[0] && ($(".form-group--float").each(function() {
  0 == !$(this).find(".form-control").val().length && $(this).find(".form-control").addClass("form-control--active")
}),
$("body").on("blur", ".form-group--float .form-control", function() {
  0 == $(this).val().length ? $(this).removeClass("form-control--active") : $(this).addClass("form-control--active")
}),
$(this).find(".form-control").change(function() {
  0 == !$(this).val().length && $(this).find(".form-control").addClass("form-control--active")
}))


/*----- Funciones para facilitar la vida -----*/
var a,today,data,table, data_inventario,u, btn_def, requerimientos = [];
function date(id) {
  today = new Date().toISOString().split('T')[0];
  $(id).val(today);
}

/*----- Valida o invalida un botón en el formulario para el evento submit -----*/
function iform(form, pass = false){
  var btn = $(form).children('[type="submit"]');
  if(!pass){
    btn_def = btn.html();
    btn.attr('disabled', true).html('Procesando...');
  } else {
    btn.removeAttr('disabled').html(btn_def);
  }
}

/*----- Dumpea cualquier respuesta de ajax, Esto también es un constante recordatorio para que hagas las notificaciones o modales -----*/
function form_dump(res){
  console.info(res);
  if(res.success){
    alert(res.msj)
    console.warn(res.msj);
  }else{
    alert('Error: ' + res.msj),
    console.error(res.msj);
  }
}

/*----- Gestión del inventario (mercancia.html) -----*/
function inventario() {
  table = $("#data-table").DataTable({
    "ajax":"api/inventario.json",
    "columnDefs": [{
      "targets": 5,
      "data": null,
      "defaultContent": '<a href="#" class="btn_table_fix actions__item zmdi zmdi-assignment-o info" "></a><a href="#" class="btn_table_fix actions__item zmdi zmdi-search buscar " "></a>'
    }],
    "columns": [
    { "data": "cod" },
    { "data": "nom" },
    { "data": "minmax" },
    { "data": "stock" },
    { "data": "stock" },
    { "data": null,"className": 'fix_table'}
    ],
    autoWidth: !1,
    responsive: !0,
    lengthMenu: [[10, 25, 50, -1], ["10 Productos.", "25 Productos.", "50 Productos.", "Todos"]],
    language: {
      searchPlaceholder: "Buscar..."
    },
    sDom: '<"dataTables__top"lfB>rt<"dataTables__bottom"ip><"clear">',
    buttons: [{
      extend: "excelHtml5",
      title: "Inventario"
    }, {
      extend: "csvHtml5",
      title: "Inventario"
    },{
      extend: "pdfHtml5",
      title: "Inventario"
    },{
      extend: "print",
      title: "Inventario"
    }],
    initComplete: function(a, b) {
      $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend('<div class="dataTables_buttons hidden-sm-down actions"> <button class="btn btn-light btn--icon-text" data-table-action="print"><i class="zmdi zmdi-print"></i> Print</button> <button class="btn btn-light btn--icon-text" data-table-action="excel"><i class="zmdi zmdi-file-text"></i> Excel</button> <button class="btn-m btn btn-primary btn--icon-text nuevo"><i class="zmdi zmdi-m zmdi-plus"></i></button></div>')
    }
  }),
  $(".dataTables_filter input[type=search]").focus(function() {
    $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
  }),
  $(".dataTables_filter input[type=search]").blur(function() {
    $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
  }),
  $("body").on("click", "[data-table-action]", function(a) {
    a.preventDefault();
    var b = $(this).data("table-action");
    if ("excel" === b && $(this).closest(".dataTables_wrapper").find(".buttons-excel").trigger("click"),
      "csv" === b && $(this).closest(".dataTables_wrapper").find(".buttons-csv").trigger("click"),
      "pdf" === b && $(this).closest(".dataTables_wrapper").find(".buttons-pdf").trigger("click"),
      "print" === b && $(this).closest(".dataTables_wrapper").find(".buttons-print").trigger("click"),
      "fullscreen" === b) {
      var c = $(this).closest(".card");
    c.hasClass("card--fullscreen") ? (c.removeClass("card--fullscreen"),
      $("body").removeClass("data-table-toggled")) : (c.addClass("card--fullscreen"),
      $("body").addClass("data-table-toggled"))
    }
  });
  
  $('#data-table').on('click','.info',function (e) {
    e.preventDefault();
    $('#modal_info').modal();
    $this = $('#modal_info #form_editar_producto');
    data = table.row($(this).parents("tr")).data();
    console.log(data)
    console.log(data.nom)
    $('#cod').val(data.codigo),
    $('#id_pro').val(data.id),
    $('#min_').val(data.min),
    $('#max_').val(data.max),
    $('#producto').val(data.nom),
    $('.producto').html(data.nom),
    $('#categoria').val(data.cat),
    $('#descripcion').val(data.des)
    $('#unidad_caja').val(data.cant),
    $('#unidad_medida').val(data.uni);
  });
  
  date("input.date");
  $(document).on('click','.nuevo',function (e) {
    e.preventDefault();
    $('#modal_nuevo').modal();
  });
  date("input.date");

  // total('.uni','.can','.total');
  function e_s (id, button,modal) {
    $(id+' tbody').on('click', 'a'+button, function (e) {
      e.preventDefault();
      $(modal).modal()
      r = table.row($(this).parents("tr"));
      data = table.row($(this).parents("tr")).data();
      console.log(data)
      $('.codigo').html(data.cod)
      $('.producto').html(data.nom)
      $('.categoria').html(data.cat)
      $('.min-max').html(data.uni)
      $('.stock').html(data.stock)
      $('.cod_pro').val(data.id)
      $(modal).find('form fieldset legend strong').html('AGREGADO POR : ' + data.usu);
      $('.reque_cantidad').submit(function(e) {
        e.preventDefault();
        $("#modal_cantidad").modal('hide')
        r.remove().draw();
      })
    })
  }
  e_s ('#data-table', '.entrada', '#modal_entrada');
  e_s ('#data-table', '.salida', '#modal_salida');

  date("input.date");

  $.getJSON('api/proveedores.json', function(proveedores) {
    for (var o = 0; o < proveedores.length; o++) {
      var pro = proveedores[o];
      $('datalist#proveedores').append(new Option(pro.nom));
    }
  });

  $('#form_editar_producto').submit(function(e) {
    e.preventDefault();
    $this = this;
    console.log($(this).serializeArray());
    iform($this);
    $.post('api/productos.json', $(this).serialize(), function(res) {
      form_dump(res);
    }).always(function(){iform($this, true); table.ajax.reload()});
  });

  $('#entrada').submit(function(e){
    e.preventDefault();
    var $this = this;
    iform($this);
    $.post('api/entradas.json', $(this).serialize() ,function(res) {
      form_dump(res);
    }).always(function(){iform($this, true); table.ajax.reload();});
  });

  $('#Salida').submit(function(e){
    e.preventDefault();
    var $this = this;
    iform($this);
    $.post('api/salidas.json', $(this).serialize(), function(res) {
      form_dump(res);
    }).always(function(){iform('#Salida', true); table.ajax.reload();});
  });

  $.getJSON('api/productos.json/categorias', function(categorias) {
    for (var i = 0; i < categorias.length; i++) {
      $('datalist#categorias').append(new Option(categorias[i].id,categorias[i].categoria));
    }
  });

  $('#nuevo_producto').submit(function(e) {
    e.preventDefault();
    var $this = this;    
    var min = parseInt($('#al_min').val()), max = parseInt($('#al_max').val()), cod = $('[name="cod"]').val();

    if(cod != '' && !/[0-9]{5,5}/.test(cod)) return alert('El codigo tener cinco (5) números');

    if(min > max) return alert('La cantidad minima no puede ser mayor que la cantidad maxima');

    iform($this);
    $.post('api/productos.json', $(this).serialize(), function(res) {
      form_dump(res);
    })
    .always(function(){ iform($this, true); table.ajax.reload()});
  });

}

/*----- Detalles producto -----*/
function entrada_salidas() {
  function n_e_s (id, button,modal) {
    $(document).on('click', button, function (e) {
      e.preventDefault();
      $(modal).modal()
      r = table.row($(this).parents("tr"));
      data = table.row($(this).parents("tr")).data();
      console.log(data)
      $('.codigo').html(data.cod)
      $('.producto').html(data.nom)
      $('.categoria').html(data.cat)
      $('.min-max').html(data.uni)
      $('.stock').html(data.stock)
      $('.cod_pro').val(data.id)
      $(modal).find('form fieldset legend strong').html('AGREGADO POR : ' + data.usu);
      $('.reque_cantidad').submit(function(e) {
        e.preventDefault();
        $("#modal_cantidad").modal('hide')
        r.remove().draw();
      })
    })
  }
  n_e_s ('#detalles_producto', '.entrada', '#modal_entrada');
  n_e_s ('#detalles_producto', '.salida', '#modal_salida');

  $.getJSON('api/productos.json', function(productos) {
    for (var i = 0; i < productos.length; i++) {
      var p = productos[i];
      $('datalist#productos').append('<option data-id="'+p.id+'" data-cod="'+p.cod+'" data-nom="'+p.nom+'" data-cat="'+p.cat+'" data-max="'+p.max+'" data-per="'+p.persona+'" data-stock="'+p.stock+'" value="'+p.nom+'"></option>');
    }
  });


  $('#buscar_producto').on('input submit', function(e){
    e.preventDefault();
    if($('datalist#productos option[value="'+this.value+'"]').val() === this.value){ 
      var input = $('datalist#productos option[value="'+this.value+'"]');
      console.log(input.data())
      $('#cod_pro').val(input.data('cod'));
      $('#nom_pro').val(input.data('nom'));
      $('#cat_pro').val(input.data('cat'));
      $('#stock_pro').val(input.data('stock'));
      $('#persona').html('AGREGADO POR : ' + input.data('per'));
      // u = 'api/entradas.json?id='+input.data('id');
      tabla_entrada.ajax.url('api/entradas.json?id='+input.data('id')).load();
      tabla_salida.ajax.url('api/salidas.json?id='+input.data('id')).load();
      $('.e_s').removeClass('hide');
    }
  });

  const tabla_entrada = $('#table_entradas').DataTable({
    ajax : '' ,
    lengthMenu: [[10, 25, 50, -1], ["10 Reg.", "25 Reg.", "50 Reg.", "Todo"]],
    searching: false,
    scrollX: true,
    sDom: '<"dataTables__top e_s hide"lfB>rt<"dataTables__bottom"ip><"clear">',
    columns : [
      { 'data' : 'cod' },
      { 'data' : 'fecha_e' },
      { 'data' : 'proveedor' },
      { 'data' : 'fecha_v' },
      { 'data' : 'precio' },
      { 'data' : 'uni' },
      { 'data' : 'cant' },
      { 'data' : 'stock' }
    ],
    buttons: [{
      extend: "excelHtml5",
      title: "Export Data"
    }, {
      extend: "csvHtml5",
      title: "Export Data"
    }, {
      extend: "print",
      title: "Material Admin"
    }],
    initComplete: function(a, b) {
      $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend('<div class="dataTables_buttons hidden-sm-down actions"> Desde : <input type="date" class="form-control form-control-sm" name="min" id="min_e_s"> Hasta : <input type="date" class="form-control form-control-sm" name="max" id="max_e_s"><button class="btn btn-primary btn--icon-text"><i class="zmdi zmdi-plus"></i>Entrada</button></div>')
    }
  });
  /* CODIGO FECHA E.  PROVEEDOR FECHA V.  PRECIO  UNIDAD  CANTIDAD  TOTAL */
  /* id cod fecha_s estatus unidad cantidad descripcion total */
  const tabla_salida = $('#table_salidas').DataTable({
    ajax : '',
    columns : [
      { 'data' : 'cod' },
      { 'data' : 'fecha_s' },
      { 'data' : 'usu'},
      { 'data' : 'fecha_s'},
      { 'data' : 'descripcion'},
      { 'data' : 'unidad'},
      { 'data' : 'cantidad'},
      { 'data' : 'total'}
    ]
  });

  $('#mostrar_todo').click(function(e){
    e.preventDefault();
    $('#detalles_producto').remove();
    var table = $('#table_entradas').DataTable({
      ajax : "api/entradas.json",
      destroy : true,
      columns : [
        { 'data' : 'cod' },
        { 'data' : 'fecha_e' },
        { 'data' : 'proveedor' },
        { 'data' : 'fecha_v' },
        { 'data' : 'precio' },
        { 'data' : 'uni' },
        { 'data' : 'cant' },
        { 'data' : 'stock' }
      ]
    });
    $('#table_entradas_wrapper').removeClass('dataTables_wrapper');
  });
}

/*----- Realiza el seguimiento de un producto -----*/
function seguimiento() {
  // TODO: Validar
  $(document).on('click', '.table-seguimiento tbody tr', function (e) {
    $("#modal_seguimiento").modal()
    function status_p(id) {
      $(id).on('click', function (e) {
        e.preventDefault();
        $(id).parents("li").addClass("active");
      });
    }
    status_p('#transporte')
    status_p('#entregado')
  });

  $.getJSON('api/salidas.json', function(salidas){
    for (var i = 0; i < salidas.length; i++) {
      salida = salidas[i];
      $('.table-seguimiento').find('tbody').append(`
        <tr>
          <td>${salida.fecha_s}</td>
          <td>${salida.cod}</td>
          <td>${salida.total} KG</td>
          <td>
            <ul class="nav nav-tabs process-model more-icon-preocess">
              <li class=""><i class="zmdi zmdi-local-store" aria-hidden="true"></i>
                <p>En espera</p>
              </li>
              <li class=""><i class="zmdi zmdi-local-shipping" aria-hidden="true"></i>
                <p>En camino</p>
              </li>
              <li class=""><i class="zmdi zmdi-assignment-check" aria-hidden="true"></i>
                <p>Entregado</p>
              </li>
            </ul>
          </td>
        </tr>`);
    }
  });
}

/*----- Requerimientos -----*/
function requerimiento() {
  // Tabla 
  tabla_reque = $('#table_reque').DataTable({
    ajax : "api/productos.json",
    destroy : true,
    "columnDefs": [{
      "targets": 1,
      "data": null,
      "defaultContent": '<a href="#" class="btn_table_fix actions__item zmdi zmdi-plus " "></a>'
    }],
    "columns": [
    { "data" : "nom" },
    { "data" : null,"className": 'fix_table'}
    ]
  });

  tabla_confirmar = $('#table_confirmar').DataTable();
  // Llamado del modal
  $('#table_reque tbody').on('click', 'a.zmdi-plus', function (e) {
    e.preventDefault();
    $("#modal_cantidad").modal()
    r = tabla_reque.row($(this).parents("tr"));
    data = tabla_reque.row($(this).parents("tr")).data();
    $('#req_uni').html(data.uni);
    if(data.stock == 0){
      $("#modal_cantidad").modal('hide');
    }
    $('#r_cantidad').attr({
      min : 1,
      max: data.stock
    });
  });
  // Llamado del modal enviar
  $('#enviar').click( function (e) {
    e.preventDefault();
    $("#modal_enviar").modal();
    date("input.date");  
  });
  
  // Investigar sobre el evento close en los modales de datatable
  $('#reque_cantidad').submit(function(e) {
    e.preventDefault();
    if($("#table_confirmar tbody tr td:contains('No hay datos disponibles')")){
      $("#table_confirmar tbody tr td:contains('No hay datos disponibles')").remove()
    }
    $('#table_confirmar tbody').append('<tr><td>'+data.nom+'</td><td>'+$('#r_cantidad').val()+'</td></tr>');
    requerimientos.push({"can" : $('#r_cantidad').val(), "nom" : data.nom});
    console.info(requerimientos);
    $('#r_cantidad, #req_uni').empty();
    $("#modal_cantidad").modal('hide')
    r.remove().draw();
  })    
}

/*----- Registro de un producto -----*/
function nuevo(){
  $.getJSON('api/productos.json/categorias', function(categorias) {
    for (var i = 0; i < categorias.length; i++) {
      $('datalist#categorias').append(new Option(categorias[i].id,categorias[i].categoria));
    }
  });

  $('#nuevo_producto').submit(function(e) {
    e.preventDefault();
    var $this = this;    
    var min = parseInt($('#al_min').val()), max = parseInt($('#al_max').val()), cod = $('[name="cod"]').val();

    if(cod != '' && !/[0-9]{5,5}/.test(cod)) return alert('El codigo tener cinco (5) números');

    if(min > max) return alert('La cantidad minima no puede ser mayor que la cantidad maxima');

    iform($this);
    $.post('api/productos.json', $(this).serialize(), function(res) {
      form_dump(res);
    })
    .always(function(){ iform($this, true); });
  });
}
