(function(window, document){
  spa.getID('view').enrutar()
  .ruta('/', 'pages/home', null, null)
  .ruta('/inventario', 'pages/inventario', null, function(){inventario();})
  .ruta('/nuevo', 'pages/nuevo', null, function(){nuevo();})
  .ruta('/producto_detalles', 'pages/producto_detalles', null, function(){entrada_salidas();})
  .ruta('/requerimiento', 'pages/requerimiento', null, function(){requerimiento();})
  .ruta('/seguimiento', 'pages/seguimiento', null, function(){seguimiento();})
})(window, document);