(function(window, document){
  'use strict';

  var inicio = function (){
    var elemento = null,
    marco = null,
    rutas = {},
    controladores = {},
    ctrlActual = null,

    spa = {
      getID: function(id){
        elemento = document.getElementById(id);
        return this;
      },
      get: function (id) {
        return document.getElementById(id);
      },
      noSubmit: function(){
        elemento.addEventListener('submit', function(e){
          e.preventDefault();
        }, false);
        return this;
      },
      controlador: function(nombre, ctrl) {
        controladores[nombre] = {'controlador': ctrl}
      },
      getCtrl: function(){
        return ctrlActual
      },
      enrutar: function(){
        marco = elemento;
        return this;
      },
      ruta: function(ruta, plantilla, controlador, carga){
        rutas[ruta] = {
          'plantilla': plantilla,
          'controlador': controlador,
          'carga': carga
        }
        return this;
      },
      manejadoRutas : function(){
        var hash = window.location.hash.substring(1) || '/',
        destino = rutas[hash],
        xhr = new XMLHttpRequest();
        if (destino && destino.plantilla) {
          if (destino.controlador) {
            ctrlActual = destino.controlador;
          }
          xhr.addEventListener('load', function() {
            marco.innerHTML = this.responseText;
            setTimeout (function (){
              if (typeof(destino.carga) === 'function') {
                destino.carga();
              }
            },100)
          },false);
          xhr.open('post', destino.plantilla, true);
          xhr.send(null);

        }else{
          window.location.hash = '#/';

        }
      }
    };
    return spa;       
  }
  if (typeof window.spa === 'undefined') {
    window.spa = inicio();
    window.addEventListener('load', spa.manejadoRutas, false)
    window.addEventListener('hashchange', spa.manejadoRutas, false)
  }else{
    console.log("se esta llamando la spa nuevamente");
  }
})(window, document);
