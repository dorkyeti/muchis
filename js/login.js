$('form').submit(function(e) {
	e.preventDefault();
	var btn = $(this).children('[type="submit"]'),
	btn_df = btn.html();
	btn.attr('disabled', true).html('Procesando...');
	$.post('api/usuarios.json/login', $(this).serialize(), function(res){
		console.log(res);
		if (res.success) {
			location.reload();
		} else {
			$('#msj').html(res.msj);
		}
	}).always(function(){btn.removeAttr('disabled').html(btn_df)});
});